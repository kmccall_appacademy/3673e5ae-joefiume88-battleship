class HumanPlayer

  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Please enter firing coordinates:"
    puts "ex: [0, 1]"
    position = gets.chomp.strip
    position.split(",").map! {|el| el.to_i}
  end
end
