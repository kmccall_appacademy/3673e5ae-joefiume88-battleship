require_relative 'board'
require_relative 'player'

class BattleshipGame

  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("Human"), board = Board.random)
    @board = board
    @player = player
    @hit = false
  end

  def attack(position)
    row, col = position
    if board.grid[row][col] == :s
      @hit = true
    else
      @hit = false
    end

    board.grid[row][col] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def hit?
    @hit
  end

  def play_turn
    position = nil

    until valid_position?(position)
      display_status
      position = player.get_move
    end

    attack(position)
  end

  def play
    play_turn until game_over?

    puts "you won!"
  end

  def valid_position?(position)
    position.is_a?(Array) && board.in_range?(position)
  end

  def display_status
    system("clear")
    board.display
    if hit?
      puts "It's a hit!"
    else
      puts "Miss"
    end
    puts "There are #{count} ships remaining."
  end
end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
