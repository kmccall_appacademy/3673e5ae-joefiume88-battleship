class Board

  DISPLAY_HASH = {
    nil => " ",
    :s => " ",
    :x => "x"
  }

  def self.default_grid
    # grid = []
    # row = Array.new(10)
    # 10.times { grid << row }
    # grid
    Array.new(10) { Array.new(10) }
  end

  def self.random
    self.new(self.default_grid, true)
  end

  attr_reader :grid

  def initialize(grid = self.class.default_grid, random = false)
    @grid = grid
    randomize if random
  end

  def count
    grid.flatten.select { |el| el == :s}.length
  end

  def empty?(position = nil)
    row, col = position
    if position
      grid[row][col] == nil
    else
      count == 0
    end
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def place_random_ship
    raise "Error, board is full" if full?
    pos = random_position

    until empty?(pos)
      pos = random_position
    end

    row_rand, col_rand = pos
    grid[row_rand][col_rand] = :s
  end

  def random_position
    [rand(grid.length), rand(grid.length)]
  end

  def won?
    grid.flatten.none? { |el| el == :s}
  end

  def display
    header = (0..9).to_a.join(" ")
    puts "  #{header}"
    grid.each_with_index { |row, i| display_row(row, i) }
  end

  def display_row(row, i)
    chars = row.map { |el| DISPLAY_HASH[el] }.join(" ")
    puts "#{i} #{chars}"
  end

  def randomize(count = 10)
    count.times { place_random_ship }
  end

  def in_range?(position)
    position.all? { |x| x.between?(0, grid.length - 1) }
  end
end
